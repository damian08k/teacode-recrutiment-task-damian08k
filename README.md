# Basic information

This project contains a recruitment task for TeaCode company.

## Technologies

The whole project was created in React technology based on Create React App. In addition, I used:

-   react-lazyload library to create lazy loading data (https://www.npmjs.com/package/react-lazyload)
-   Material UI to create app graphic view/design.

# How to run the project locally

If you want to run your project locally follow the steps below:

1. Download or clone repository on your computer
    - if you want to download a project, remember that you have to unzip the archive you downloaded
2. Turn on your favourite IDE
3. Find and open a project folder in IDE
4. Open the terminal
    - if you're using Visual Studio Code you can type key-shortcut "ctrl + ~" to open terminal inside VSCode
    - if you're using classic a terminal, find a path to the project folder
5. Type "npm install" in a terminal and press Enter
6. When the installation is complete, you can type "npm start" to run the developer server, where you can see how applications work in the browser
    - default port for CRA is 3000, so if app doesn't open automatically, you can copy "http/localhost:3000" and paste it into browser search-bar
