import React from "react";

import CheckboxProvider from "./contextAPI/CheckboxContext";
import DataProvider from "./contextAPI/DataContext";
import SearchProvider from "./contextAPI/SearchContext";

import Header from "./components/header/Header";
import Main from "./components/main/Main";
import Search from "./components/search/Search";

import Box from "@material-ui/core/Box";

import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";

const theme = createMuiTheme({
    spacing: 5,
});

const API = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";

const App = () => {
    return (
        <Box display="flex" flexDirection="column">
            <DataProvider>
                <ThemeProvider theme={theme}>
                    <Header />
                    <CheckboxProvider>
                        <SearchProvider>
                            <Search />
                            <Main API={API} />
                        </SearchProvider>
                    </CheckboxProvider>
                </ThemeProvider>
            </DataProvider>
        </Box>
    );
};

export default App;
