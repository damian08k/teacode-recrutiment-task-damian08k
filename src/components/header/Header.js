import React from "react";

import Box from "@material-ui/core/Box";
import teal from "@material-ui/core/colors/teal";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
    fontAttrs: {
        fontSize: "2rem",
        fontWeight: "700",
        color: "white",
    },
});

const Header = () => {
    const pageTitle = "Contact";
    const headerBgColor = teal[600];
    const headingClasses = useStyles();

    return (
        <Box component="header" py={2} bgcolor={headerBgColor}>
            <Typography variant="h1" align="center" className={headingClasses.fontAttrs}>
                {pageTitle}
            </Typography>
        </Box>
    );
};

export default Header;
