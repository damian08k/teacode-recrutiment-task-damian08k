import React, { useContext, useEffect } from "react";

import { DataContext } from "../../contextAPI/DataContext";
import { SearchContext } from "../../contextAPI/SearchContext";

import Box from "@material-ui/core/Box";

import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";

import { FormControl, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
    formWidth: {
        width: "100%",
    },

    inputPadding: {
        padding: "20px 5px",
    },

    inputFontSize: {
        fontSize: "20px",
    },
});

const Search = () => {
    const { data } = useContext(DataContext);
    const { inputValue, setInputValue, setSearchData } = useContext(SearchContext);
    const classes = useStyles();

    const handleInputChange = evt => setInputValue(evt.target.value);

    useEffect(() => {
        const currentInputValue = inputValue.toLowerCase();
        const searchData = [];

        for (let i = 0; i < data.length; i++) {
            const firstName = data[i].first_name.toLowerCase();
            const lastName = data[i].last_name.toLowerCase();

            if (firstName.startsWith(currentInputValue) || lastName.startsWith(currentInputValue)) {
                searchData.push(data[i]);
            }
        }

        setSearchData(searchData);
    }, [inputValue]);

    return (
        <Box component="nav">
            <FormControl component="form" noValidate className={classes.formWidth}>
                <TextField
                    InputProps={{
                        classes: {
                            input: classes.inputFontSize,
                        },
                        startAdornment: (
                            <InputAdornment position="start" className={classes.inputPadding}>
                                <SearchIcon fontSize="large" />
                            </InputAdornment>
                        ),
                    }}
                    value={inputValue}
                    onChange={handleInputChange}
                ></TextField>
            </FormControl>
        </Box>
    );
};

export default Search;
