import React, { useContext } from "react";

import ConditionalWrapper from "./ConditionalWrapper";

import { CheckboxContext } from "../../contextAPI/CheckboxContext";
import { SearchContext } from "../../contextAPI/SearchContext";

import LazyLoad from "react-lazyload";

import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Checkbox from "@material-ui/core/Checkbox";
import ListItem from "@material-ui/core/ListItem";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
    liAttrs: {
        padding: "10px",
        borderBottom: "1px solid gray",
    },

    headingAttrs: {
        fontSize: "1.8rem",
        fontWeight: "700",
    },

    pAttrs: {
        fontSize: "1.5rem",
    },

    checkboxAttrs: {
        padding: "10px",
        marginLeft: "10px",
    },
});

const Contact = ({ dataCollection }) => {
    const { checkedBoxes, setCheckedBoxes } = useContext(CheckboxContext);
    const { inputValue } = useContext(SearchContext);
    const classes = useStyles();

    const handleItemClick = itemId => {
        checkedBoxes.includes(itemId)
            ? setCheckedBoxes(checkedBoxes.filter(box => box !== itemId))
            : setCheckedBoxes([...checkedBoxes, itemId]);
    };

    const ListOfContacts = dataCollection.map(({ id, avatar, first_name, last_name, email }) => (
        <ConditionalWrapper
            key={id}
            condition={!inputValue}
            wrapper={children => (
                <LazyLoad key={id} placeholder="Loading...">
                    {children}
                </LazyLoad>
            )}
        >
            <ListItem key={id} onClick={() => handleItemClick(id)} className={classes.liAttrs}>
                <Box display="flex" flexWrap="wrap" width="100%">
                    <Avatar src={avatar} alt={`${first_name} ${last_name} avatar`} />
                    <Box display="flex" flexDirection="column" justifyContent="center" pl={2}>
                        <Typography variant="h2" className={classes.headingAttrs}>
                            {`${first_name} ${last_name}`}
                        </Typography>
                        <Typography className={classes.pAttrs}>{email}</Typography>
                    </Box>
                    <Checkbox className={classes.checkboxAttrs} checked={checkedBoxes.includes(id)}></Checkbox>
                </Box>
            </ListItem>
        </ConditionalWrapper>
    ));

    return ListOfContacts;
};

export default Contact;
