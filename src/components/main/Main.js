import React, { useContext } from "react";

import Contact from "./Contact";

import { CheckboxContext } from "../../contextAPI/CheckboxContext";
import { DataContext } from "../../contextAPI/DataContext";
import { SearchContext } from "../../contextAPI/SearchContext";

import useFetch from "../../hooks/useFetch";

import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
    headingAttrs: {
        fontSize: "1.8rem",
        fontWeight: "700",
    },

    ulAttrs: {
        padding: "0",
        listStyleType: "none",
    },
});

const Main = API => {
    const { data, isLoad } = useContext(DataContext);
    const { checkedBoxes } = useContext(CheckboxContext);
    const { searchData, inputValue } = useContext(SearchContext);
    const classes = useStyles();

    useFetch(API);

    const LoadingMessage = (
        <Typography variant="h2" className={classes.headingAttrs}>
            Loading data...
        </Typography>
    );

    const showContactCondition = inputValue ? (
        <Contact dataCollection={searchData} />
    ) : (
        <Contact dataCollection={data} />
    );

    const showUsersOrLoadingMessage = isLoad ? showContactCondition : LoadingMessage;

    return (
        <Box component="main" height="auto">
            <List className={classes.ulAttrs}>{showUsersOrLoadingMessage}</List>
            {checkedBoxes.length > 0 && console.log("All selected IDs: " + checkedBoxes.join(", "))}
        </Box>
    );
};

export default React.memo(Main);
