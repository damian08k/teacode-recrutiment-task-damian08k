import { useContext, useEffect } from "react";

import { DataContext } from "../contextAPI/DataContext";

const useFetch = ({ API }) => {
    const { setIsLoad, setData } = useContext(DataContext);

    useEffect(() => {
        const abortController = new AbortController();

        fetch(API, { signal: abortController.signal })
            .then(resolution => {
                if (!resolution.ok) {
                    throw new Error("Something's wrong with data. I could not fetch the data from thar resource.");
                }

                return resolution.json();
            })
            .then(data => {
                data.sort((firstElement, secondElement) =>
                    firstElement.last_name.localeCompare(secondElement.last_name)
                );
                setData(data);
                setIsLoad(true);
            })
            .catch(error => {
                console.error(error.message);
            });

        return () => abortController.abort();
    }, [API, setData, setIsLoad]);

    return [];
};

export default useFetch;
