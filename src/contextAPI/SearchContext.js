import React, { createContext, useState } from "react";

export const SearchContext = createContext();

const SearchProvider = props => {
    const [inputValue, setInputValue] = useState("");
    const [searchData, setSearchData] = useState([]);

    return (
        <SearchContext.Provider value={{ inputValue, searchData, setInputValue, setSearchData }}>
            {props.children}
        </SearchContext.Provider>
    );
};

export default SearchProvider;
