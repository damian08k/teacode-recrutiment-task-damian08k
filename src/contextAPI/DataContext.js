import React, { createContext, useState } from "react";

export const DataContext = createContext();

const DataProvider = props => {
    const [data, setData] = useState([]);
    const [isLoad, setIsLoad] = useState(false);

    return <DataContext.Provider value={{ data, setData, isLoad, setIsLoad }}>{props.children}</DataContext.Provider>;
};

export default DataProvider;
