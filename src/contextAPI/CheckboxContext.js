import React, { createContext, useState } from "react";

export const CheckboxContext = createContext();

const CheckboxProvider = props => {
    const [checkedBoxes, setCheckedBoxes] = useState([]);

    return (
        <CheckboxContext.Provider value={{ checkedBoxes, setCheckedBoxes }}>{props.children}</CheckboxContext.Provider>
    );
};

export default CheckboxProvider;
